defmodule FeedClient.CallApi do

  def get_all_postlist(page_no, per_page) do

    Neuron.Config.set(url: "http://write-cms.invo.zone:9002/graphiql")

      {:ok, response} = Neuron.query("""

      {
        getPosts(pageNo:#{page_no}  perPage: #{per_page})
        {
          firstPage
          hasNext
          hasPrev
          list
          {
            active
            anonymous
            content
            groupId
            id
            insertedAt
            media {
              id
              src
              active
              order
              post_id
              height
              width
            }
            polls {
              id
            }
            sensitive
            title
            updatedAt
            userId
          }
          nextPage
          nextPage
          page
          prevPage

        }
      }
    """)
      IO.inspect response, label: "here is a response"
      string_map = response.body["data"]["getPosts"]
      final_data =  keys_to_atoms(string_map)
      # IO.inspect final_data, label: "here is final_data"
      final = final_data
      |> Enum.map(fn result ->
        case result do
          {:list, data}->
            data
            |> Enum.map(fn data_result ->
              # IO.inspect Enum.at(data_result.media, 0)
              active_media_check = Enum.at(data_result.media, 0)
              case active_media_check do
                nil ->
                  # case data_result.anonymous do
                  #   false ->
                      new = Map.merge(data_result, %{group_name: get_group_name(data_result.group_id)} )
                      new = get_user_name(new)
                      get_reactions(new)

                    # _ ->


                  # end
                _ ->
                  case active_media_check.active do
                  true ->
                    # case data_result.anonymous do
                      # false ->
                        new = Map.merge(data_result, %{group_name: get_group_name(data_result.group_id)} )
                        new = get_user_name(new)
                        get_reactions(new)

                      # _ ->


                    # end
                  end
              end
            end)
          {_, data}->
            data
        end
      end)
      # |> Enum.reject(&is_nil/1)
      # IO.inspect Enum.at(final, 3) |> Enum.reject(&is_nil/1) , label: "here is final"
      list = Enum.at(final, 3) |> Enum.reject(&is_nil/1)
      IO.inspect %{
        first_page: Enum.at(final, 0),
        has_next: Enum.at(final, 1),
        has_prev: Enum.at(final, 2),
        list: list,
        next_page: Enum.at(final, 4),
        page: Enum.at(final, 5),
        prev_page: Enum.at(final, 6),
      }, label: "here is last response"



  end
  ######################## getting Reactions with post ids ####################
  def get_reactions(result_map) do
    # IO.inspect result_map.id
    case result_map do
      [] ->
        nil

      "" ->
        nil
      _ ->

        Neuron.Config.set(url: "http://write-cms.invo.zone:8050/graphql")

        case Neuron.query("""
        {
          totalReactionCount(postId:"#{result_map.id}")

        }
        """)
        do
          {:ok, res} ->

            totalReaction = res.body["data"]["totalReactionCount"]
            merge_result = Map.merge(result_map, %{total_reactions: totalReaction})
            merge_result
          {:error, _res} ->
            totalReaction = "Reaction module not working"
            merge_result = Map.merge(result_map, %{total_reactions: totalReaction})
            merge_result
        end
    end
  end
  ######################## getting user names with user ids ####################

  def get_user_name(result_map) do

    case result_map do
      [] ->
        nil

      "" ->
        nil
      _ ->

        Neuron.Config.set(url: "http://3.22.187.247:5091/graphiql")

        case Neuron.query("""
          {
            profile(authId:"",
            id:"#{result_map.user_id}") {
              fullname
              username
              profilePhoto
            }
          }
        """)
        do
        {:ok, response} ->


        username = response.body["data"]["profile"]["username"]
        pic = response.body["data"]["profile"]["profilePhoto"]
        full = response.body["data"]["profile"]["fullname"]
        merge_result = Map.merge(result_map, %{user_pic: pic, user_name: username, full_name: full})
        merge_result
        {:error, _response} ->
          username = "profile module not working"
          pic = "profile module not working"
          full = "profile module not working"
          merge_result = Map.merge(result_map, %{user_pic: pic, user_name: username, full_name: full})
          merge_result
        end
    end
  end




  ################################ getting names with group ids ###################

  def get_group_name(list_of_id) do
    case list_of_id  do
      [] ->
        []
      "" ->
        []
      _ when is_list(list_of_id) ->
        for_data =
        for data <-  list_of_id do

          Neuron.Config.set(url: "http://3.22.187.247:5081/graphiql")

          case Neuron.query("""
          {
            group(id: "#{data}")
            {
              name
            }
          }
          """)
          do
          {:ok, response} ->
            response.body["data"]["group"]["name"]
          {:error, _response} ->
            "group module not working"
          end
        end
        for_data
    end
  end
##################################################################################

  def get_group_postlist(id, page_no, per_page) do

    Neuron.Config.set(url: "http://write-cms.invo.zone:9002/graphiql")

      {:ok, response} = Neuron.query("""
      {
        getGroupPosts(groupId: "#{id}" pageNo:#{page_no}  perPage: #{per_page})
        {
          firstPage
          hasNext
          hasPrev
          list
          {
            active
            anonymous
            content
            groupId
            id
            insertedAt
            media {
              id
              src
              active
              order
              post_id
              height
              width
            }
            polls {
              id
            }
            sensitive
            title
            updatedAt
            userId
          }
          nextPage
          nextPage
          page
          prevPage

        }
      }
    """)

      string_map = response.body["data"]["getGroupPosts"]
      final_data =  keys_to_atoms(string_map)

      final = Enum.at(final_data, 0)
      |> Enum.map(fn result ->
        case result do
          {:list, data}->
            data
            |> Enum.map(fn data_result ->
              new = get_user_name(data_result)
              get_reactions(new)
            end)
          {_, data}->
            data
        end
      end)
      # final = final_data
      # |> Enum.map(fn result ->
      #     new = get_user_name(result)
      #     get_reactions(new)

      # end)
      %{
        first_page: Enum.at(final, 0),
        has_next: Enum.at(final, 1),
        has_prev: Enum.at(final, 2),
        list: Enum.at(final, 3),
        next_page: Enum.at(final, 4),
        page: Enum.at(final, 5),
        prev_page: Enum.at(final, 6),
      }


    end

    def get_user_postlist(id, page_no, per_page) do

      Neuron.Config.set(url: "http://write-cms.invo.zone:9002/graphiql")



        {:ok, response} = Neuron.query("""
        {
          getUsersPost(userId: "#{id}" pageNo:#{page_no}  perPage: #{per_page})
          {
            firstPage
            hasNext
            hasPrev
            list
            {
              active
              anonymous
              content
              groupId
              id
              insertedAt
              media {
                id
                src
                active
                order
                post_id
                height
                width
              }
              polls {
                id
              }
              sensitive
              title
              updatedAt
              userId
            }
            nextPage
            nextPage
            page
            prevPage

          }
        }
      """)

        string_map = response.body["data"]["getUsersPost"]
        final_data =  keys_to_atoms(string_map)


        # final = final_data
        # |> Enum.map(fn result ->
        #   # IO.inspect result.group_id
        #   new = Map.merge(result, %{group_name: get_group_name(result.group_id)} )
        #   get_reactions(new)

        # end)

        final = Enum.at(final_data, 0)
        |> Enum.map(fn result ->
          # IO.inspect result
          case result do
            {:list, data} ->
              data
              |> Enum.map(fn data_result ->
                # IO.inspect data_result, label: "result_Data"
                new = Map.merge(data_result, %{group_name: get_group_name(data_result.group_id)} )
                get_reactions(new)
              end)
            {_, data} ->
              data
          end
        end)

        %{
          first_page: Enum.at(final, 0),
          has_next: Enum.at(final, 1),
          has_prev: Enum.at(final, 2),
          list: Enum.at(final, 3),
          next_page: Enum.at(final, 4),
          page: Enum.at(final, 5),
          prev_page: Enum.at(final, 6),
        }





      end




    ########################  Data re-arranger ##########################

    def keys_to_atoms(string_key_map) when is_map(string_key_map) do
      for {key, val} <- string_key_map, into: %{}, do: {String.to_atom(Recase.to_snake(key)), keys_to_atoms(val)}
    end

    def keys_to_atoms(string_key_list) when is_list(string_key_list) do
      string_key_list
      |> Enum.map(&keys_to_atoms/1)

    end

    def keys_to_atoms(value), do: value
end
