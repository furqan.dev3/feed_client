defmodule FeedGraphql.GraphQL.Schema do

  use Absinthe.Schema


  object :post_data1 do
    field :first_page, :integer
    field :has_next, :boolean
    field :has_prev, :boolean
    field :list, list_of(:all_post)
    field :next_page, :integer
    field :page, :integer
    field :prev_page, :integer
  end

  object :post_data2 do
    field :first_page, :integer
    field :has_next, :boolean
    field :has_prev, :boolean
    field :list, list_of(:group_list)
    field :next_page, :integer
    field :page, :integer
    field :prev_page, :integer
  end

  object :post_data3 do
    field :first_page, :integer
    field :has_next, :boolean
    field :has_prev, :boolean
    field :list, list_of(:user_list)
    field :next_page, :integer
    field :page, :integer
    field :prev_page, :integer
  end

  object :all_post do

    field :id, :string
    field :user_id, :string
    field :active, :boolean
    field :anonymous, :boolean
    field :content, :string
    field :group_id, list_of(:string)
    field :inserted_at, :string
    field :media, list_of(:medias)
    field :polls, :poll
    field :sensitive, :boolean
    field :title, :string
    field :user_name, :string
    field :user_pic, :string
    field :group_name, list_of(:string)
    field :updated_at, :string
    field :total_reactions, :string
    field :full_name, :string

  end


  object :group_list do

    field :id, :string
    field :user_id, :string
    field :active, :boolean
    field :anonymous, :boolean
    field :content, :string
    field :group_id, list_of(:string)
    field :inserted_at, :string
    field :media, list_of(:medias)
    field :polls, :poll
    field :sensitive, :boolean
    field :title, :string
    field :updated_at, :string
    field :total_reactions, :string
    field :user_pic, :string
    field :user_name, :string
    field :full_name, :string
  end

  object :user_list do

    field :id, :string
    field :user_id, :string
    field :active, :boolean
    field :anonymous, :boolean
    field :content, :string
    field :group_id, list_of(:string)
    field :inserted_at, :string
    field :media, list_of(:medias)
    field :polls, :poll
    field :sensitive, :boolean
    field :title, :string
    field :updated_at, :string
    field :total_reactions, :string
    field :group_name, list_of(:string)
  end

  object :poll do
    field :id, :string
  end

  object :medias do
    field :id, :string
    field :src, :string
    field :active, :boolean
    field :post_id, :string
    field :order, :integer
    field :height, :string
    field :width, :string
  end

  query do
    @desc "it will return list of all posts
      EX:
      query
        {
          getPosts(pageNo:4  perPage: 5)
          {
            firstPage
            hasNext
            hasPrev
            list
            {
              active
              anonymous
              content
              groupId
              id
              insertedAt
              media {
                id
              }
              polls {
                id
              }
              sensitive
              title
              updatedAt
              userId
            }
            nextPage
            nextPage
            page
            prevPage

          }
        }"

      field :feed_list_all_posts, :post_data1   do
        arg :page_no, non_null(:integer)
        arg :per_page, non_null(:integer)
        resolve fn _,%{page_no: page_no, per_page: per_page}, _ ->
          list = FeedClient.CallApi.get_all_postlist(page_no, per_page)
          # IO.inspect list, label: "here is list"
          {:ok,  list}
        end
      end


      @desc "it will return list of posts of groups
      EX:
      query{
        feedListGroupPosts (id: '547bd541-880b-4bd0-8b41-3e19e23f3064'){

          active
          anonymous
          content
          id
          media {
            id
          }
           groupId
          polls {
            id
          }
          sensitive
          title
          insertedAt
          updatedAt
          userId
      }

      }"

      field :feed_list_group_posts, :post_data2 do
        arg :page_no, non_null(:integer)
        arg :per_page, non_null(:integer)
        arg :id, non_null(:string)
        resolve fn _, %{id: id, page_no: page_no, per_page: per_page}, _ ->
          list = FeedClient.CallApi.get_group_postlist(id, page_no, per_page)
          {:ok,  list}
        end
      end

      @desc "it will return list of posts of specific user
      EX:
      query{
        feedListUserPosts (id: 'ad29e49a-5e61-43cf-ab85-438f2ee9fb16'){

          active
          anonymous
          content
          id
          media {
            id
          }
           groupId
          polls {
            id
          }
          sensitive
          title
          insertedAt
          updatedAt
          userId
      }

      }"

      field :feed_list_user_posts, :post_data3 do
        arg :page_no, non_null(:integer)
        arg :per_page, non_null(:integer)
        arg :id, non_null(:string)
        resolve fn _, %{id: id, page_no: page_no, per_page: per_page}, _ ->
          list = FeedClient.CallApi.get_user_postlist(id, page_no, per_page)
          IO.inspect list
          {:ok,  list}
        end
      end


  end
end
